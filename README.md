#  Population Dynamics Model Cartography

[**DISCLAIMER: This project is in a draft state, expect typos and big changes ahead**]

This repository holds the code and data to build an [interactive
cartography of population dynamics
model](https://geeklhem.gitlab.io/popdyn-model-db/). This work is
motivated by collaborations with mathematicians, biologists and
philosophers in evolutionary biology.

It started with a doodle in my notebook about what it meant to look at
"long term growth rates" (and in some extent, fitness) in a few
classic models, and what were the relationship between them. This
doodle quickly evolved in this fun interactive graph. 

When working in inter-disciplinary research, I realised that most
people think about several kinds of models all the time and
explicitly or implicitly switch between them without much
problem... until they discover that they are not talking about the
same thing at all. I hope this will help build common ground between
disciplines. 

The goal of the project is not to be exhaustive about models or
results about them, it's just meant to be a tool to "see the big
picture".

I am particularly curious about how this view relate to your own
mental map of population dynamics models. Feel free to share any
insights and feedback with me !

## Under the hood 

There are two kinds of conceptual objects in this database, `models`
that are the vertices of the graph, and `transforms` that are directed
edges between `models`. Each model is has a unique universal
identifier (`uuid`), each transformation is identified by an ordered
pair of `uuid`. 

Here is how the repository is organised: 

- `data/` contains all the data about the models and transformations. 
   + `data/models.csv` contains the metadata about all models (title,
     tags, uuid...).
   + `data/transforms.csv` contains the metadata about all
     transformations.
   + `data/models/*.md` contains long description of models in a easy
     to edit (and display) format.
   + `data/transforms/*.md` contains long description of transformations in a
     easy to edit (and display) format.
- `make.py` reads all the data from the `data` folder and build a
  `www/data.json` file that is used by the html/javascript render.
- `www/` contains the visualization web-page. 
   + `www/index.html` is the bare-bone html template, it calls `script.js`
   + `www/script.js` reads `data.json` and build the visualization.
   + `www/style.css` is a minimal style-sheet. 

The code uses the python libraries `pandas` (csv parsing), `pypandoc`
(convert markdown to html using the `pandoc` software) and the
javascript library `d3js` (for the interactive visualization). Thanks for free and open source software !
   
## Contributing

If you have suggestions, additions or corrections feel free to let me
know using the issue tracker on gitlab, or directly sending me an
email. Forks and merge request are welcome !

## Re-use

The code and data in this repository is distributed under Creative-Commons
CC0, and thus waived all copyright and related or neighbouring rights
to the extent possible under law.

Feel free to re-use all of part of the content of this repository for
any research, entertainment or educational purpose. Attribution is not
required, but always appreciated. If you make something cool with it, let me
know !
