function ForceGraph({
    nodes, // an iterable of node objects (typically [{id}, …])
    links // an iterable of link objects (typically [{source, target}, …])
}, {
    nodeId = d => d.uuid, // given d in nodes, returns a unique identifier (string)
    nodeGroup = d => d.malthusian?0:1, // given d in nodes, returns an (ordinal) value for color
    nodeGroups, // an array of ordinal values representing the node groups
    nodeTitle = d=>d.title,  // given d in nodes, a title string
    nodeFill = "currentColor", // node stroke fill (if not using a group color encoding)
    nodeStroke = "#fff", // node stroke color
    nodeStrokeWidth = 1.5, // node stroke width, in pixels
    nodeStrokeOpacity = 1, // node stroke opacity
    nodeRadius = 10, // node radius, in pixels
    nodeStrength,
    linkSource = d => d.from, // given d in links, returns a node identifier string
    linkTarget = d => d.to, // given d in links, returns a node identifier string
    linkStroke = "#999", // link stroke color
    linkStrokeOpacity = 0.6, // link stroke opacity
    linkStrokeWidth = 2, // given d in links, returns a stroke width in pixels
    linkStrokeLinecap = "round", // link stroke linecap
    linkStrength,
    colors = d3.schemeTableau10, // an array of color strings, for the node groups
    width = 640, // outer width, in pixels
    height = 640, // outer height, in pixels
    invalidation // when this promise resolves, stop the simulation
} = {}) {

    const bool_tags = ['deterministic','malthusian','discrete-time','multi-type','atomic-individual']
    const bool_tag_color = d3.scaleOrdinal([true,false], ["#007bff", "#6c757d"])
    const family_color = d3.scaleOrdinal([],  d3.schemeCategory10)
    const N = d3.map(nodes, nodeId)
    const LS = d3.map(links, linkSource)
    const LT = d3.map(links, linkTarget)
    if (nodeTitle === undefined) nodeTitle = (_, i) => N[i]
    const T = nodeTitle == null ? null : d3.map(nodes, nodeTitle)
    const C = d3.map(nodes, d=>d.html)
    const LC = d3.map(links, d=>d.html)
    const LH = d3.map(links, d=>d.title)
    const G = nodeGroup == null ? null : d3.map(nodes, nodeGroup).map(intern)
    const W = typeof linkStrokeWidth !== "function" ? null : d3.map(links, linkStrokeWidth)
    const L = typeof linkStroke !== "function" ? null : d3.map(links, linkStroke)

    // Replace the input nodes and links with mutable objects for the simulation.
    nodes = d3.map(nodes, (data, i) => ({id: N[i], data:data}))
    links = d3.map(links, (data, i) => ({source: LS[i], target: LT[i], data:data}))
    
    // Compute default domains.
    if (G && nodeGroups === undefined) nodeGroups = d3.sort(G)

    // Construct the scales.
    const color = nodeGroup == null ? null : d3.scaleOrdinal(nodeGroups, colors)

    // Construct the forces.
    const forceNode = d3.forceManyBody()
    const forceLink = d3.forceLink(links).id(({index: i}) => N[i])
    if (nodeStrength !== undefined) forceNode.strength(nodeStrength)
    if (linkStrength !== undefined) forceLink.strength(linkStrength)

    // Create the structure
    const div = d3.create("div").attr("class","viz")
    const svg_container = div.append('div').attr("id","svg_container")
    const legend = svg_container.append('div')
    const display = div.append('div').attr('id','display')
    const title = display.append('h1')
    const metadata = display.append('div')
    const content = display.append('div')
    
    
    const simulation = d3.forceSimulation(nodes)
	  .force("link", forceLink)
	  .force("charge", forceNode)
	  .force("center",  d3.forceCenter())
	  .on("tick", ticked)

    const svg = svg_container.append("svg")
	  .attr("width", width)
	  .attr("height", height)
	  .attr("viewBox", [-width / 2, -height / 2, width, height])
	  .attr("style", "max-width: 100%; height: auto; height: intrinsic;")
	  .on('click', _=> {window.location.hash=''; welcome()})
    const link = svg.append("g")
	  .attr("stroke", typeof linkStroke !== "function" ? linkStroke : null)
	  .attr("stroke-opacity", linkStrokeOpacity)
	  .attr("stroke-width", typeof linkStrokeWidth !== "function" ? linkStrokeWidth : null)
	  .attr("stroke-linecap", linkStrokeLinecap)
	  .selectAll("line")
	  .data(links)
	  .join("line")

    const node = svg.append("g")
	  .attr("fill", nodeFill)
	  .attr("stroke", nodeStroke)
	  .attr("stroke-opacity", nodeStrokeOpacity)
	  .attr("stroke-width", nodeStrokeWidth)
	  .selectAll("g")
	  .data(nodes)
	  .join("g")
	  .append("circle")
	  .attr("r", d=> nodes[d.index].data.fundamental? 1.5*nodeRadius : (nodes[d.index].data.core? nodeRadius : 0.5*nodeRadius))
	  .call(drag(simulation))


    link.attr("stroke-dasharray", d=>{switch(d.data.type){
	case "new-assumption":
	return "1"
	case "special-case":
	return "2 4"
	case "continuous-time":
	return "6 10 "
	case "restrict":
	//return "5 10"
	default:
	return "1"
    }})
    
    if (W) link.attr("stroke-width", ({index: i}) => W[i])
    if (L) link.attr("stroke", ({index: i}) => L[i])
    if (G) node.attr("fill", ({index: i}) => color(G[i]))
    if (T) node.append("title").text(({index: i}) => T[i])
    if (T) node.append("text").text(({index: i}) => T[i])

    if (invalidation != null) invalidation.then(() => simulation.stop())
    
    
    function color_by(tag, color_scheme){
	node.attr("fill", ({index: i}) => color_scheme(nodes[i].data[tag]))
	if (color_scheme.domain().length == 2){
	    legend.html(`<span class="tag" style="background-color:${color_scheme(true)}">${tag}</span><span class="tag" style="color:${color_scheme(false)}">not ${tag}</span>`)
	} else{
	    legend.html(tag+": "+color_scheme.domain().map(e=>`<span class="tag" style="background-color:${color_scheme(e)}">${e}</span>`).join(' ') )}
    }
    
    
    function mouseover(event, datum){
	if (datum){window.location.hash = datum.data.uuid}
	title.html(T[datum.index])
	console.log(datum.index)
	metadata.html('')
	let taglist = metadata.append('ul')
	taglist.selectAll('li')
	    .data(bool_tags)
	    .join('li')
	    .attr('class', 'tag')
	    .text(d=>nodes[datum.index].data[d]?d:"not "+d)
	    .style('background-color', d=>bool_tag_color(nodes[datum.index].data[d]))
	    .on('click', (e,d) => color_by(d, bool_tag_color))
	taglist.append('li').text(nodes[datum.index].data['family'])
	    .attr('class', 'tag')
	    .style('background-color', family_color(nodes[datum.index].data['family']))
	    .on('click', _ => color_by('family', family_color))


	function display_link(inbound, link, node){
	    console.log('displaylink(i,l,n)', inbound, link, node)
	    var li = d3.create('li')
	    li.append('span').text(inbound?"From ":"To ")
	    li.append('strong').text(node.data.title)
	    	.attr('class','tag')
		.style('background-color', family_color(node.data.family)+'55')
	    li.append('span').text(' by ')
	    li.append('a').text(link.data.title)
	    li.append('span').text(" ("+link.data['type']+")")
		.attr('class','tip')
		//.style('background-color',"#ccc")
	    return li.node().innerHTML
	}
	
	//console.log(links)
	content.html(C[datum.index])
	let follow = content.append("div").attr("class","card transforms")
	follow.append('h4').text('Links to other models')
	follow.append('ul').selectAll('li')
	    .data(links.filter(d=>d.source.index==datum.index))
	    .join('li')
	    .html(d=>display_link(false, links[d.index], nodes[d.target.index]))
//		"To <strong class='tag>"+T[d.target.index]+"</strong> by  <a>"+LH[d.index]+"</a> <span class='tag' style='background-color:#ccc'>"+d.data['type']+"</span>")
	    .on('click', mouseover_link)
	follow.append('ul').selectAll('li')
	    .data(links.filter(d=>d.target.index==datum.index))
	    .join('li')
	    .html(d=>display_link(true, links[d.index], nodes[d.source.index]))
//	    .html(d=>"From <strong>"+T[d.source.index]+"</strong> by <a>"+LH[d.index]+"</a> <span class='tag'  style='background-color:#ccc'>"+d.data['type']+"</span>")
	    .on('click', mouseover_link)
	content.append('div').attr('class','tip').text('Hover a node to display it, click a tag to change the color-scheme, click the graph to come back to the welcome page.')
    }
    function mouseover_link(event, datum){
	//title.html(LH[datum.index])
	//metadata.html('From '+T[datum.source.index]+' to '+T[datum.target.index])
	//content.html(LC[datum.index])
	content.html('')
	title.html('')
	metadata.html('')
	let from = content.append('div').attr("class","card from")
	    .on('click', _=>mouseover(undefined,nodes[datum.source.index]))
	let link = content.append('div').attr("class","card link")
	let to = content.append('div').attr("class","card to")
	    .on('click', _=>mouseover(undefined,nodes[datum.target.index]))

	from.append('h2').text("From ")
	to.append('h2').text("To ")
	from.append('a').text(T[datum.source.index])
	to.append('a').text(T[datum.target.index])
	link.append('h2').text(LH[datum.index])
	link.append('div').html(LC[datum.index])
    }
    
    node.on("mouseover",  mouseover)
    link.on("mouseover",  mouseover_link)
    
    
    function intern(value) {
	return value !== null && typeof value === "object" ? value.valueOf() : value
    }

    function ticked() {
	link
	    .attr("x1", d => d.source.x)
	    .attr("y1", d => d.source.y)
	    .attr("x2", d => d.target.x)
	    .attr("y2", d => d.target.y)
	
	node
	    .attr("cx", d => d.x)
	    .attr("cy", d => d.y)
    }

    function drag(simulation) {    
	function dragstarted(event) {
	    if (!event.active) simulation.alphaTarget(0.3).restart()
	    event.subject.fx = event.subject.x
	    event.subject.fy = event.subject.y
	}
    
	function dragged(event) {
	    event.subject.fx = event.x
	    event.subject.fy = event.y
	}
	
	function dragended(event) {
	    if (!event.active) simulation.alphaTarget(0)
	    event.subject.fx = null
	    event.subject.fy = null
	}	
	return d3.drag()
	    .on("start", dragstarted)
	    .on("drag", dragged)
	    .on("end", dragended)
    }

    function welcome(){
	title.html('')
	metadata.html('')
	content.html('')
	color_by('family', family_color)
	content.append('h3').html('Explore mathematical models of population dynamics !')
	content.append('p').attr('class','tip').html('Hover a node to see the model, hover an edge to see how to go from one model to the other.  Read about the <a href="https://gitlab.com/geeklhem/popdyn-model-db/-/blob/master/README.md">goals and technical side</a> of this work, feel free to <a href="https://gitlab.com/geeklhem/popdyn-model-db/">contribute</a> and <a href="https://gitlab.com/geeklhem/popdyn-model-db/-/issues">discuss issues</a>.')
	content.append('h4').text('Select a color scheme:')
	let categories = content.append('div').append('dl')
	categories.append('dt').text('Family:')
	    .on('click', _=> color_by('family', family_color))
	categories.append('dd').selectAll('span').data(family_color.domain())
	    .join('span').style('background-color',family_color).attr('class','tag').text(d=>d)
	    .on('click', _=> color_by('family', family_color))
	bool_tags.forEach(tag=>{
            categories.append('dt').text(tag+":")
	    	.on('click', (e,d)=> color_by(tag, bool_tag_color))
	    categories.append('dd').selectAll('span').data(bool_tag_color.domain())
		.join('span').style('background-color',bool_tag_color).attr('class','tag')
		.text(d=>(d?"":"not ")+tag)
		.on('click', (e,d)=> color_by(tag, bool_tag_color))
	})
	content.append('h4').text('Select a model:')
	let modellist = content.append('ul')
	modellist.selectAll('li').data(nodes).join('li')
	    .attr('class','tag')
	    .text(d=>d.data['title'])
	    .style('font-weight', d=>d.data['core']?'bold':'normal')
	    .style('background-color', d=>family_color(d.data['family']))
	    .on('click', mouseover)

    }
    if (window.location.hash){
	let initial_node = nodes.find(node => node.data.uuid == window.location.hash.slice(1))
	console.log('hs', window.location.hash.slice(1), initial_node)
	if (initial_node != undefined){
	    color_by('family', family_color)
	    mouseover(undefined, initial_node)
	}
    }else{
        welcome()
    }
   
    return div
}


var graph 
d3.json("data.json").then(data => {
    console.log(data)
    graph = ForceGraph(data)
    d3.select('main').node().appendChild(graph.node())
})
