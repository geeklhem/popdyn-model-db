---
title: Variable Geometric Growth
uuid: 4f4d0a53-71a7-470f-9b8e-3832a5e17e4e
---

Let $n_t$ be the number of individuals at time $t=1,2\ldots$, $W(t)$
be the multiplicative growth rate at time $t$.

$$n_{t+1} = W(t) n_t $$

The change in $W$ does not depend on the population composition, as
such this is more adapted to model extrinsic effects such as seasonal cycles.

## Malthusian parameter

There is no Malthusian parameter in general. However, for a fixed
period $(0,t)$, the equivalent constant geometric growth rate is the
geometric mean $W = \left (\prod_{i=0}^t W_i \right)^{1/t}$.

$$n_t = n_0 W_0 W_1...W_t = n_0 \prod_{i=0}^t W_i = n_0W^t$$

## Extinction

There is extinction if there is $t$ such that $W(t)=0$.

