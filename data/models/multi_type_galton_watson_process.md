---
title: Multi-type Galton-Watson Process
uuid: a4240fa9-11cf-44b8-b2c7-c8f98bde8588
---

Let: 

- $Z_t = (Z_{t1}, Z_{t2},\ldots Z_{tk})$ be the number of individuals
  of type $1,2 \ldots k$ at time $t$,
- $\xi^{(j)} = (\xi^{(j)}_1,\xi^{(j)}_2,\ldots,\xi^{(j)}_k)$ be i.i.d. random
  variables corresponding to the number of offspring of an individual
  of type j individual in one unit of time.

$$Z_{t+1} = \sum_{j=1}^k \sum_{i=1}^{Z_{tj}} \xi_i^{(j)}$$

## Malthusian parameter 

Let $M$ be the mean matrix, with element $m_{ij}$ being the expected
number of offspring of type $i$ produced by an individual of type $j$
in one unit of time. 

The Malthusian parameter is $\rho$ the dominant eigenvalue of $M$ (if it
exists).

Indeed, the expected trajectory follow a Matrix population model, with
projection matrix $M$ (Haccou et al. 2007, p24):

$$\mathbb E(Z_t) = \mathbb E(Z_0) M^t,$$

Similarly, for a supercritical, irreducible process, the population
is asymptotic to a geometric growth with rate $\rho$ multiplied by a
positive random variable W, and stable class distribution $u$ (Haccou
et al. 2007, p156):

$$Z_t \sim W\rho^t u$$

## Extinction

The probability of extinction in the long run, $\lim_{t \to
\infty}\mathbb P(Z_t=0)$ is equal to 1 if $m<1$, and lower than 1 if
$m>1$. It can be explicitly computed for some values of $\xi$. 

