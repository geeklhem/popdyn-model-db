---
title: Exponential Growth
uuid: ae776b3d-5e75-4f42-b93b-1d1d7f72c173
---

Let $n(t)$ be the number of individuals at time $t$, $r$ is the
exponential growth rate. 

$$\frac{dn}{dt} =  rn$$

## Malthusian parameter 

The exponential growth rate $r$ is the Malthusian parameter of the
system by definition.

$$n(t) =  n(0)e^{rt}$$

## Extinction 

There is no strict extinction if $r \neq 0$, but the population tends toward 0 if
$r<0$.

