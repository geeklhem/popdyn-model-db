---
title: Ricker Model
uuid: ec5bed80-bc15-4d38-ad2d-5ca02875a3eb
---

Let $z_t$ be the number of individuals at time $t=1,2\ldots$, $m$ be
the average number of offspring of an adult, and $e^{-bz_t}$ be the
proportion of juvenile that survive to reproductive age. 

$$z_{t+1} = m z_t e^{-bz_t} $$
