---
title: Lotka-Volterra Model
uuid: 6c10f8e1-f9e8-43e0-8847-629cb52286e0
---

Let:

- $n_i(t)$ be the population size of type $i$ at time $t$,
- $r_i$ be the intrinsic growth rate of type $i$ and,
- $a_{ij}$ be the density-dependant effect of type $j$ on type $i$.

$$ \frac{dn_i}{dt} = r_i n_i \left(1 - \sum_{j=1}^{k} a_{ij} n_j\right) $$

## Malthusian Parameter 


