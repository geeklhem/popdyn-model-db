---
title: Wright-Fisher Process
uuid: 713d925c-fdc4-430e-8095-f5d3e9ca549f
---

Let $N$ be the constant population size. At each new generation, the
parent of each individual is drawn at random form the previous
generation and inherits its type.

Let $X_t$ be be the number of individuals of type $A$ in the
population at time $t$. $X_t$ is a markov chain with state space
$[0,N]$ with two absorbing states in 0 and 1.
