---
title: Logistic Map
uuid: 97112a76-84c5-456a-89cb-5c56a70097d8
---

$$n_{t+1} = r n_t (1-n_t)$$

This equation exhibits oscillations and deterministic chaos for value of $r > 3$.

## Malthusian Parameter

There is no Malthusian parameter, but in the low-density limit ($(1-n_t) \approx 0$) the
population grow geometrically with parameter $r$.

## Extinction 

There is no strict extinction but the population tend toward 0 if $r<1$.
