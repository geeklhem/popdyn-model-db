---
title: Cannings Model
uuid: 3d32b919-55ff-4b5f-9ba5-4b567f4dd91f
---

Let:

- N be the constant population size.
- $v_i(j)$ be the number of children of individual $i$ at generation $j$.
- $v(j)$ be the vector $(v_1(j), v_2(j))$

With two properties:

- **Equilibrium**: $(v(j))_{j>0}$ be i.i.d. random variables.
- **Exchangeability**: Each v(j) is exchangeable, meaning that its probability is the same for any permutation.  

Then,

$$Y(j+1) = \sum_{k=1}^{Y(j)} v_k$$

With $Y(j)$ be the number of individual of a type of interest at generation $j$.
