---
title: Moran Process
uuid: 8a3858dc-06f8-4348-bfd3-6b9a24e2451e
---

Let N be the constant population size. Let $X_t$ be the number of
individuals of type $A$ at time $t$, and $N-X_t$ be the number of
individuals of type B at time $t$. Let $r_B$ be the fitness of type $B$
and $r_A$ be the fitness of type $A$. 

At each time step, an individual reproduce and replace another one. 

$$P_{i,i+1} = \frac{r_A i}{r_a i + r_b (N-i)} \frac{N-i}{N}$$

$$P_{i,i-1} = \frac{r_B (N-i)}{r_a i + r_b (N-i)} \frac{i}{N}$$

$$P_{i,i} = 1 - P_{i,i+1} - P_{i,i-1}$$
