---
title: Matrix Population Model
uuid: f5880657-b333-4ae9-81ac-e9e892024d86
---

Let 

- $n_i^{(t)}$ be the number of individual of type $i$ individuals at time $t$.  $\mathbf{n}^{(t)}$ in vector form.
- $s_{ij} \in [0,1]$ the fraction of individual that survives and transition from class $i$ to age class $j$ in an unit of time. They are the elements of the matrix $\mathbf{S}$.
- $f{ij}>0$ the number of individuals of type $j$ produced by an individual of type $i$ in an unit of time. They are the elements of the matrix $\mathbf{F}$.
- $\mathbf{n^(t)}$ the column vector of $(n_i)_{i=1,2,3...}$
- $\mathbf{A} = \mathbf{S}+ \mathbf{F}$ be the population projection matrix. 

$$ \mathbf{n}^{(t+1)} = \mathbf{A} \mathbf{n}^{(t)} $$

## Malthusian parameter 

The dominant eigenvalue $\lambda$ of $\mathbf{A}$ is Malthusian
parameter (if it exists, see the Perron-Froebenius theorem) of the
model, in the sense that the total population $n(t)$ is asymptotic to
a geometric growth with parameter $\lambda$ as $t$ tends toward
$+\infty$ (Keyfitz et al. 2007, p160):

$$ n(t) \sim \lambda^t $$

## Extinction 

There is no strict extinction, but the population tends toward 0 if
$\lambda<1$.

