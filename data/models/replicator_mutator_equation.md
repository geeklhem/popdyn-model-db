---
title: Replicator-Mutator Equation
uuid: 935c942d-ba80-400e-bd94-637227d8e73a
---

Let:

- $x_i$ be the frequency of individual of type $i$, $i \in (1,2,\ldots,n)$
- $f_i$ be the intrinsic growth rate of individuals of type $i$
- $Q_{ij}$ be the transtition probability from type $i$ to $j$
- $\phi(x)$ be the average intrinsic growth rate in the population $\phi(x) = \sum_{j=1}^n x_i f_i$.

$$\frac{x_i}{dt} = \sum_{j=1}^n x_j f_j(x)Q_{ij} - \phi(x) x_i $$


References 
(Nowak 2006, p272)
