---
title: Logistic Growth with Allee effect
uuid: 383dd747-e662-427b-b980-9cd21d2db330
discrete_time: false
deterministic: true
malthusian: false
density-dep: true
core: false 
---

Let $n(t)$ be the number of individuals at time $t$, $r>0$ be the
exponential growth rate, $K$ be the carrying capacity, $k$ be the
minimal viable population.

$$\frac{dn}{dt} =  r(\frac{n}{k}-1)(1-\frac{n}{K})$$

## Malthusian Parameter

There is no Malthusian parameter, but in the low-density limit
($(1-\frac{n}{K}) \approx 1$) and above the minimal population size
$(\frac{n}{k}-1\approx n/k)$, the population grow exponentially with parameter $r/k$.

## Extinction 

There is no strict extinction but the population tend toward 0 if $n(t)<k$.
