---
title: MacKendrick-von Foerster model
uuid: 7bc35a9e-5c8d-4565-a191-843182c61743
---

Let:

- $n(t,x)$ be the population density with age structure $x\in (0,\infty)$ at time $t\in(0,\infty)$
- B(x) the birth rate

$$\frac{\partial}{\partial t} n(t,x) +  \frac{\partial}{\partial x} n(t,x) = 0, $$

$$n(t,x=0) = \int B(y)n(t,y)dy, $$

$$n(t=0,x) = n^0(x).$$

## Malthusian parameter 

Under some assumptions on B, (positivity, regularity, integrability),
there exists a Malthusian parameter $\lambda_0$ that is an eigenvalue
associated to the operator of the renewal equation(Perthame et al. 2006 p.57)
