---
title: Linear Birth-Death Process
uuid: be132b3c-dbb5-4a02-a8f7-756075e1363e
---

Let:

- $Z(t)$ be the number of individuals at time $t \in \mathbb R$.
- $b>0$ the (constant) birth rate of individuals
- $d>0$ the (constant) death rate of individuals

## Malthusian parameter

The Malthusian parameter is $r=(b-d)$ for at least two reasons.

The expected trajectory follow an exponential growth with parameter $r$:  

$$\mathbb E(Z(t)) = e^{(b-d)t}$$

