---
title: Deterministic Annual Plant Growth Model
uuid: 9a0a8504-58a9-4839-aae8-e12a9e43e998
---

$$N(t+1) = N(t) \left [ \gamma (1-g) + \frac{fg}{1+\alpha g N(t)}  \right ]$$

With:

- $N(t)$ the number of individuals at time t,
- $g$ the fraction seeds that germinate annually from the seed bank
- $\gamma$ the expected fraction of seeds that survive annually in the seed bank
- $f$ the expected annual seed production of established individuals 
- $\alpha$ negative density dependance parameter
