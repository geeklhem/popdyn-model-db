---
title: Leslie Model
uuid: dd7b31dd-9200-45ab-b8e6-255786c53bb6
---

Also called age-structured (matrix) models. 

Let:

- $n_x^{(t)}$ be the number of individuals of age $x$ at time $t$. 
- $s_x$ the fraction of individuals that survives from age class $x$ to age class $x+1$
- $f_x$ the per-capita fecundity of individuals of class x.
- $\mathbf{n^{(t)}}$ the column vector of $(n_i)_{i=1,2,3...}$
- L the population projection matrix such that: 

$$ L = \begin{bmatrix} f_0 & f_1 & f_2 & \ldots & f_{\omega - 2} & f_{\omega - 1} \\ s_0 & 0 & 0 & \ldots & 0 & 0\\ 0 & s_1 & 0 & \ldots & 0 & 0\\ 0 & 0 & s_2 & \ldots & 0 & 0\\ \vdots & \vdots & \vdots & \ddots & \vdots & \vdots\\ 0 & 0 & 0 & \ldots & s_{\omega - 2} & 0 \end{bmatrix} $$

$$\mathbf{n}^{(t+1)} = \mathbf{L} \mathbf{n}^{(t)}$$

## Malthusian parameter 

The dominant eigenvalue $\lambda$ of $\mathbf{L}$ is the Malthusian
parameter of the model, in the sense that the total population $n(t)$
is asymptotic to a geometric growth with parameter $\lambda$ as $t$
tends toward $+\infty$:

$$ n(t) \sim \lambda^t $$

## Extinction 

There is no strict extinction, but the population tends toward 0 if
$\lambda<1$.
