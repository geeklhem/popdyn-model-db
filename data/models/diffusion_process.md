---
title: Diffusion Process
uuid: 3a0252ad-43c6-4a1c-b682-3fb51769d5bb
---

Let $Z_t$ be the density of individuals at time t, and $W$ be the standard Brownian motion.

$$
dZ_t = \sqrt{\beta Z_t}dW_t + \alpha Z_t dt 
$$
