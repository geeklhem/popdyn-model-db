---
title: Geometric Growth
uuid: 0756f8da-1a95-41f4-a0f0-9f3ca4f53d50
---

Let $n_t$ be the number of individuals at time $t=1,2\ldots$, $w$ be the multiplicative growth rate. 

$$n_{t+1} = w n_t$$

## Malthusian parameter 

The multiplicative growth rate $w$ is the Malthusian parameter of the
system by definition.

$$n_t = n_0 w^{t}$$

## Extinction 

There is no strict extinction if $w \neq 0$, but the population tends toward 0 if
$w<1$.
