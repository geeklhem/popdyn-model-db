---
title: Quasi-Species Equation
uuid: 62119c1c-dcbb-4f80-9c1e-05d7b153d1aa
---

Let: 

- $n_i(t)$ be the number of individuals of type $i$ at time $t$
- $w_{ij}$ be the number of offspring of type $j$ that an individual of type $i$ has in a small time $dt$.


$$\frac{dn_i}{dt} = \sum_{j=1}^n w_{ij}n_j$$
