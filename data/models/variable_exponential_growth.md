---
title: Variable exponential growth
uuid: aba211e2-d973-4897-b524-8e5dba318379
---

Let $n(t)$ be the number of individuals at time $t$, $r(t)$
be the exponential growth rate at time $t$.

$$\frac{dn}{dt} = r(t) n $$

The change in $r$ does not depend on the population composition, as
such this is more adapted to model extrinsic effects such as seasonal cycles.

## Malthusian parameter

There is no Malthusian parameter in general. However, for a fixed
period $(0,t)$, the equivalent constant exponential growth rate is the
arithmetic mean $r = \frac{1}{t}\int_0^t r(x)dx$.

$$n(t) = n(0) + \int_0^t n(x) r(x) dx = n(0) e^{t \frac{1}{t} \int_0^t r(x)dx }$$

## Extinction

There is extinction if there is $t$ such that $n(t)=0$.

