---
title: Random iid Geometric Growth
uuid: 766ba4ac-c025-40e7-a44f-c5525234048f
---

Let $W_t$ for $t = 1,2,\ldots...$ be a set of independent and
identically distributed random variables. Let $Z_t$ be the population size at time $t$.

$$Z_{t+1} = W_t Z_t$$

Note that the population size has no effect. This model could be used
to model extrinsic stochasticity due to the environment


## Malthusian parameter 

Let $m$ be the expected value of $W$. The Malthusian parameter is $m$
because the expected value of a product of i.i.d variables is the
product of their expected values, so the expected trajectory grow
geometrically with rate $m$:

$$\mathbb E (Z_{t}) = \mathbb E (Z_0) \mathbb E (\prod_{i=0}^t W_i) =
\mathbb E (Z_0) m^t $$


## Extinction 

The probability of extinction is $1-(1-\mathbb P(W=0))^t$
