---
title: Logistic Growth
uuid: 62c39a81-9195-437b-b395-106511dcc346
---

Let $n(t)$ be the number of individuals at time $t$, $r$ be the
exponential growth rate, $K$ be the carrying capacity.

$$\frac{dn}{dt} =  rn(1-\frac{n}{K})$$

## Malthusian Parameter

There is no Malthusian parameter, but in the low-density limit ($(1-\frac{n}{K}) \approx 0$) the
population grow exponentially with parameter $r$.

## Extinction 

There is no strict extinction if $r \neq 0$ but the population tend toward 0 if $r<0$.
