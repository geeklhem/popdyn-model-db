---
title: Galton-Watson Process
uuid: da8a3a68-b002-439d-a646-aba05c319d72
---

Also called the simple branching process, or the
Bienaymé–Galton–Watson processes.

Let:

- $Z_t$ be the number of individuals at time $t$,
- $\xi_{j}^{(t)}$ be i.i.d. random variables corresponding to the number of offspring of the j-th individual at time t, for all values of $j=1,2\ldots$ and $t=1,2\ldots$.
- $m$ be the expected value of $\xi$

$$Z_{t+1}=\sum_{j=1}^{Z_t}\xi_{j}^{(t)}$$

## Malthusian parameter 

The Malthusian parameter is $m$ for at least two reasons: 

The expected trajectory of the model follow a geometric growth with
parameter $m$ (Haccou et al. 2007, p 18):

$$\mathbb E(Z_t) = \mathbb E(Z_0) m^t.$$

Moreover, if $m>1$, any non-extinct trajectory follow a geometric
growth with parameter $m$:

$$\lim_{t \to \infty} \frac{Z_t}{m^t} = W,$$

where $W$ is a random variable with expected value 1, and that is
equal to 0 iff the process dies out (Haccou et al. 2007, p 154).

## Extinction 

The process can go extinct if an individual has a non-null probability
to leave no offspring $\mathbb P(\xi = 0) > 0$.

The probability of extinction in the long run, $\lim_{t \to
\infty}\mathbb P(Z_t=0)$ is equal to 1 if $\rho<1$, and lower than 1
if $\rho>1$ (Haccou et al. 2007 p123). Its value depends on the type
of the founding individual, and the dominant eigenvalue and associated
eigenvector.

