---
title: Replicator Equation
uuid: 6d6edf99-77f3-4913-8863-a174fea73492
---

Let: 

- $x_i$ be the proportion of type $i$ in the population at time $t$,
- $f_i(x)$ be the growth rate of type $i$ when the population is in state $x$, 
- $\phi(x) = \sum^n_{j=1}x_jf_j(x)$ is the average population fitness. 

$$ \frac{d x_i}{dt} = x_i \left [ f_i(x) - \phi(x) \right ]$$
