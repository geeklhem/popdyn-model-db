---
title: Density dependant growth
uuid: 5746700e-b498-40ff-880c-88fb7e541708
---

Let $n_t$ be the number of individuals at time $t=1,2\ldots$, $W(n)$
be the multiplicative growth rate for a density of $n$.

$$n_{t+1} = W(n_t) n_t$$

## Malthusian parameter 

There is no Malthusian parameter in general, but if variations in
$W(n)$ are negligible so that it can be considered constant (typically
at low density), then the model is a geometric growth with rate $W$. 
