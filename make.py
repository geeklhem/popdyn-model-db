"""Collect all the data in data/ into www/data.json"""
import glob
import json
import pandas as pd
import pypandoc

table_models = "data/models.csv"
table_transforms = "data/transforms.csv"
files_models = glob.glob("data/models/*.md")
files_transforms = glob.glob("data/transforms/*.md")


def parse_md(file):
    node = {'file':file}
    parsed = pypandoc.convert_file(file, 'html', extra_args=['--mathml'])
    meta = json.loads(pypandoc.convert_file(file, 'json'))['meta']
    node['html'] = parsed
    for k,v in meta.items():
        if v['t'] == 'MetaBool':
            node[k] = v['c'] == 'True'
        elif v['t'] == 'MetaInlines':
            node[k] = ' '.join([x['c'] for x in v['c'] if 'c' in x])
    if 'uuid' in node:
        return node['uuid'], node
    else:
        return (node['from'],node['to']), node

# Parse markdown files
nodes_files = dict([parse_md(file) for file in files_models])
# Parse the csv
database = pd.read_csv(table_models).set_index('uuid')
database.dropna(subset='title', inplace=True)
nodes_table = {k:v.to_dict() for k,v in database.iterrows()}

# Fuse the two
for uuid in nodes_table:
    if uuid in nodes_files:
        nodes_table[uuid].update(nodes_files[uuid])
    else:
        title =  nodes_table[uuid]['title']
        name = ''.join([str(x).lower() if (str.isalnum(x)) else '_' for x in title])
        with open(f'data/models/{name}.md','w') as file:
            file.write("\n".join(['---',f"title: {title}",f"uuid: {uuid}", "---"]))
        print(f"New file {name}")
for uuid in nodes_files:
    if uuid not in nodes_table:
        print(f"Warning: UUID {uuid} is not in {table_models} ")
        nodes_table[uuid] = nodes_files[uuid]

# Prep output
for k in nodes_table:
    nodes_table[k]['uuid'] = k
nodes = list(nodes_table.values())


# Parse the md
links_files = dict([parse_md(file) for file in files_transforms])
# Parse the csv
database_transforms = pd.read_csv(table_transforms).set_index(['from','to'])
database_transforms.dropna(subset='title', inplace=True)
database_transforms.fillna({'html':''}, inplace=True)
links_table = {k:v.to_dict() for k,v in database_transforms.iterrows()}

# Fuse
for key in links_table:
    if key in links_files:
        links_table[key].update(links_files[key])
for key in links_files:
    if key not in links_table:
        print(f"Warning: link {key} {links_files[key]['title']} is not in {table_transforms} ")
        links_table[key] = links_files[key]

# Prep output
for k1,k2 in links_table:
    links_table[k1,k2]['from'] = k1
    links_table[k1,k2]['to'] = k2
links = list(links_table.values())

# Save the database
with open('www/data.json','w') as fh:
    json.dump({'nodes':nodes, 'links':links}, fh)
